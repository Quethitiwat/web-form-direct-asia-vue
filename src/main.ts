import { createApp } from 'vue'
import App from './App.vue'
import "ant-design-vue/dist/antd.css";
import 'bootstrap/dist/css/bootstrap.min.css';

createApp(App).mount('#app')
